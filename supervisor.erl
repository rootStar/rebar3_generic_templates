%%% @author   {{author_name}} <{{author_email}}>
%%% @copyright  {{copyright_year}}  {{author_name}}.
%%% @doc      {{description}}

-module({{name}}).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

%% Helper macro for declaring children of supervisor
-define(CHILD(I, Type), #{
    id => I,
    start => {I, start_link, []},
    restart => permanent,
    shutdown => 5000,
    type => Type,
    modules => [I]
}).

-define(CHILD(I, Type, Args), #{
    id => I,
    start => {I, start_link, Args},
    restart => permanent,
    shutdown => 5000,
    type => Type,
    modules => [I]
}).

%% ===================================================================
%% API functions
%% ===================================================================

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% ===================================================================
%% Supervisor callbacks
%% ===================================================================

init([]) ->
    {ok, { #{strategy => one_for_one, intensity => 5, period => 10}, [] } }.
